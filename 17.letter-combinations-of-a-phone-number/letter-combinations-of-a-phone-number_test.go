package main

import (
	"fmt"
	"testing"
)

func TestLetterCombinations(t *testing.T) {
	tt := []struct {
		input1  string
		output1 []string
	}{
		{"23", []string{"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"}},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := letterCombinations(tc.input1)
			t.Logf("\nexp:%v\ngot:%v", tc.output1, ret1)
		})
	}
}
