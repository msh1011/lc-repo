package main

/*
 * @lc app=leetcode id=17 lang=golang
 *
 * [17] Letter Combinations of a Phone Number
 *
 * https://leetcode.com/problems/letter-combinations-of-a-phone-number/description/
 *
 * algorithms
 * Medium (44.18%)
 * Likes:    2943
 * Dislikes: 355
 * Total Accepted:    498.8K
 * Total Submissions: 1.1M
 * Testcase Example:  '"23"'
 *
 * Given a string containing digits from 2-9 inclusive, return all possible
 * letter combinations that the number could represent.
 *
 * A mapping of digit to letters (just like on the telephone buttons) is given
 * below. Note that 1 does not map to any letters.
 *
 *
 *
 * Example:
 *
 *
 * Input: "23"
 * Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
 *
 *
 * Note:
 *
 * Although the above answer is in lexicographical order, your answer could be
 * in any order you want.
 *
 */

// @lc code=start

var dial = map[rune][]byte{'2': []byte{'a', 'b', 'c'}, '3': []byte{'d', 'e', 'f'}, '4': []byte{'g', 'h', 'i'}, '5': []byte{'j', 'k', 'l'}, '6': []byte{'m', 'n', 'o'}, '7': []byte{'p', 'q', 'r', 's'}, '8': []byte{'t', 'u', 'v'}, '9': []byte{'w', 'x', 'y', 'z'}}

func letterCombinations(digits string) []string {
	ret := []string{}
	if digits == "" {
		return ret
	}
	ret = []string{""}
	for _, c := range digits {
		var tmp []string
		cArr := dial[c]
		for j := 0; j < len(ret); j++ {
			for cI := 0; cI < len(cArr); cI++ {
				tmp = append(tmp, ret[j]+string(cArr[cI]))
			}
		}
		ret = tmp
	}
	return ret
}

// @lc code=end
