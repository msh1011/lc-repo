package main

import (
	"fmt"
	"testing"
)

func Test_generateParenthesis(t *testing.T) {
	tt := []struct {
		input1  int
		output1 []string
	}{
		{9, []string{""}},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := generateParenthesis(tc.input1)
			t.Logf("\nexp:%v\ngot:%v", tc.output1, ret1)
		})
	}
}
