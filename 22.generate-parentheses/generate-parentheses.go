package main

/*
 * @lc app=leetcode id=22 lang=golang
 *
 * [22] Generate Parentheses
 *
 * https://leetcode.com/problems/generate-parentheses/description/
 *
 * algorithms
 * Medium (58.81%)
 * Likes:    3823
 * Dislikes: 215
 * Total Accepted:    444.2K
 * Total Submissions: 754.2K
 * Testcase Example:  '3'
 *
 *
 * Given n pairs of parentheses, write a function to generate all combinations
 * of well-formed parentheses.
 *
 *
 *
 * For example, given n = 3, a solution set is:
 *
 *
 * [
 * ⁠ "((()))",
 * ⁠ "(()())",
 * ⁠ "(())()",
 * ⁠ "()(())",
 * ⁠ "()()()"
 * ]
 *
 */

// @lc code=start
func generateParenthesis(n int) []string {

	res := []string{}
	var dfs func(string, int, int)
	dfs = func(s string, l, r int) {
		if l > r {
			return
		}
		if l == 0 && r == 0 {
			res = append(res, s)
			return
		}
		if l > 0 {
			dfs(s+"(", l-1, r)
		}
		if r > 0 {
			dfs(s+")", l, r-1)
		}
	}
	dfs("", n, n)
	return res
}

// @lc code=end
