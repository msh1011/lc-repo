package main

/*
 * @lc app=leetcode id=24 lang=golang
 *
 * [24] Swap Nodes in Pairs
 *
 * https://leetcode.com/problems/swap-nodes-in-pairs/description/
 *
 * algorithms
 * Medium (47.62%)
 * Likes:    1649
 * Dislikes: 145
 * Total Accepted:    392.4K
 * Total Submissions: 822.9K
 * Testcase Example:  '[1,2,3,4]'
 *
 * Given a linked list, swap every two adjacent nodes and return its head.
 *
 * You may not modify the values in the list's nodes, only nodes itself may be
 * changed.
 *
 *
 *
 * Example:
 *
 *
 * Given 1->2->3->4, you should return the list as 2->1->4->3.
 *
 *
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func swapPairs(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	if head.Next == nil {
		return head
	}
	n1 := head
	for n1 != nil && n1.Next != nil {
		n1.Val = n1.Val + n1.Next.Val
		n1.Next.Val = n1.Val - n1.Next.Val
		n1.Val = n1.Val - n1.Next.Val
		n1 = n1.Next.Next
		if n1 == nil {
			return head
		}
	}
	return head
}

// @lc code=end
