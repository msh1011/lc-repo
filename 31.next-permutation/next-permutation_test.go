package main

import (
	"fmt"
	"testing"
)

func convert(i []int) int {
	ret := 0
	for _, n := range i {
		ret *= 10
		ret += n
	}
	return ret
}

func TestNextPermutation(t *testing.T) {
	tt := []struct {
		input1  []int
		output1 []int
	}{
		{[]int{1, 2, 3, 4, 5}, []int{1, 3, 2}},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			last := 0
			for i := 0; i < 12; i++ {
				nextPermutation(tc.input1)
				v := convert(tc.input1)
				t.Log(v)
				if v < last {
					return
				}
				last = v
			}
		})
	}
}
func TestNextPermutation1(t *testing.T) {
	tt := []struct {
		input1  []int
		output1 []int
	}{
		{[]int{1, 3, 2}, []int{2, 1, 3}},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			nextPermutation(tc.input1)
			for i := range tc.input1 {
				if tc.input1[i] != tc.output1[i] {
					t.Errorf("Error\nexp: %v\ngot: %v\n", tc.output1, tc.input1)
					return
				}
			}
		})
	}
}
