package main

import "testing"
import "fmt"
import "reflect"

func TestTwoSum(t *testing.T) {
	tt := []struct{
		input []int
		target int
		output []int
	} {
		{[]int{2,7,9,11}, 9, []int{0, 1}},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret := twoSum(tc.input, tc.target)
			if !reflect.DeepEqual(tc.output, ret) {
				t.Errorf("Incorrect Value: \nexp: %v\ngot: %v",
					tc.output, ret)
			}
		})
	}
}
