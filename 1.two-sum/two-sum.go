package main

func twoSum(nums []int, target int) []int {
	for i, t := range nums {
		for i1, t1 := range nums[i+1:] {
			if t + t1 == target {
				return []int{i, i1+i+1}
			}
		}
	}
	return nil
}
