#!/bin/bash

function join_by { local d=$1; shift; echo -n "$1"; shift; printf "%s" "${@/#/$d}"; }

files=`ls | grep "\.go"`
for file in $files; do
	folder=${file%.*}
	newFile="${folder#*.}.go"
	testFile="${folder#*.}_test.go"
	FUNC=`cat $file | grep func | cut -d '(' -f 1 | cut -d ' ' -f 2`
	FUNCU="$(tr '[:lower:]' '[:upper:]' <<< ${FUNC:0:1})${FUNC:1}"

	str=`cat $file | grep "lc code=start" -A100 | grep "func .*\(.*\)" | cut -d "(" -f 2 | cut -d ")" -f 1`
	IFS=',' read -r -a array <<< "$str"
	i=1;
	STRUCT=""
	TARGS=()
	for elem in "${array[@]}"; do
		s1=`echo $elem | cut -d " " -f 2`
		t1=`echo -e "\t\tinput$i $s1
	"`
		STRUCT="$STRUCT$t1"
		TARGS[$i]="tc.input$i"
		i=$((i+1)) 
	done
	ret=`cat $file | head -n1 | cut -d ")" -f 2 | cut -d "{" -f 1`
	ret=`cat $file | grep "lc code=start" -A100 | grep "func .*\(.*\)" | cut -d ")" -f 2 | cut -d "{" -f 1`
	IFS=',' read -r -a retarray <<< "$ret"
	j=1;
	RARGS=()
	for elem in "${retarray[@]}"; do
		s1=`echo $elem | xargs`
		t1=`echo -e "\toutput$j $s1
	"`
		STRUCT="$STRUCT$t1"
		RARGS[$i]="ret$j"
		j=$((j+1)) 
	done
	FARGS=`join_by ', ' ${TARGS[@]}`
	RetARGS=`join_by ', ' ${RARGS[@]}`
	CHECK=""
	k=1;
	for elem in "${RARGS[@]}"; do
		t1=`echo "			if (tc.output$k != $elem) {
				t.Errorf(\"Error: \\nexp:\\n%v\\ngot:\\n%v\", tc.output$k, $elem)
			}
"`
		CHECK="$CHECK$t1"
	done
	mkdir -p $folder
	echo "package main
	" | cat - $file > $folder/$newFile
	touch $folder/$testFile
	rm $file
	echo """package main

import (
	\"fmt\"
	\"testing\"
)

func Test$FUNCU(t *testing.T) {
	tt := []struct {
$STRUCT} {
		{},
	} 
	for i, tc := range tt {
		t.Run(fmt.Sprintf(\"Test-%d\", i), func(t *testing.T){
			$RetARGS := $FUNC($FARGS)
$CHECK
		})
	}
}""" > $folder/$testFile
done
