package main

import (
	"fmt"
	"testing"
)

func TestMaxArea(t *testing.T) {
	tt := []struct {
		input1  []int
		output1 int
	}{
		{[]int{1, 8, 6, 2, 5, 4, 8, 3, 7}, 49},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := maxArea(tc.input1)
			if tc.output1 != ret1 {
				t.Logf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
