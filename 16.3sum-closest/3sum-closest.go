package main

/*
 * @lc app=leetcode id=16 lang=golang
 *
 * [16] 3Sum Closest
 *
 * https://leetcode.com/problems/3sum-closest/description/
 *
 * algorithms
 * Medium (45.71%)
 * Likes:    1538
 * Dislikes: 112
 * Total Accepted:    410.1K
 * Total Submissions: 897.1K
 * Testcase Example:  '[-1,2,1,-4]\n1'
 *
 * Given an array nums of n integers and an integer target, find three integers
 * in nums such that the sum is closest to target. Return the sum of the three
 * integers. You may assume that each input would have exactly one solution.
 *
 * Example:
 *
 *
 * Given array nums = [-1, 2, 1, -4], and target = 1.
 *
 * The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
 *
 *
 */
import (
	"fmt"
	"math"
	"sort"
)

// @lc code=start
func threeSumClosest(nums []int, target int) (ret int) {
	min := math.MaxFloat64
	sort.Ints(nums)
	for i := range nums[:len(nums)-2] {
		l := i + 1
		r := len(nums) - 1
		for l < r {
			s := nums[i] + nums[l] + nums[r]
			diff := math.Abs(float64(s - target))
			if diff == 0 {
				ret = s
				return
			}
			if diff < min {
				min = diff
				ret = s
			}
			if s < target {
				l += 1
			} else {
				r -= 1
			}
		}
	}
	return
}

// @lc code=end
