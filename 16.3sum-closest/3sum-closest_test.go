package main

import (
	"fmt"
	"testing"
)

func TestThreeSumClosest(t *testing.T) {
	tt := []struct {
		input1  []int
		input2  int
		output1 int
	}{
		{[]int{-1, 2, 1, -4}, 1, 2},
		{[]int{1, 2, 4, 8, 16, 32, 64, 128}, 82, 82},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := threeSumClosest(tc.input1, tc.input2)
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
