package main

import (
	"fmt"
	"testing"
)

func TestSearch(t *testing.T) {
	tt := []struct {
		input1  []int
		input2  int
		output1 int
	}{
		{[]int{4, 5, 6, 7, 0, 1, 2}, 3, -1},
		{[]int{3, 4, 5, 6, 1, 2}, 2, 5},
		{[]int{4, 5, 6, 7, 0, 1, 2}, 2, 6},
		{[]int{}, 5, -1},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := search(tc.input1, tc.input2)
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
