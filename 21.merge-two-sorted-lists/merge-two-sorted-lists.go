package main

/*
 * @lc app=leetcode id=21 lang=golang
 *
 * [21] Merge Two Sorted Lists
 *
 * https://leetcode.com/problems/merge-two-sorted-lists/description/
 *
 * algorithms
 * Easy (50.57%)
 * Likes:    3130
 * Dislikes: 458
 * Total Accepted:    795.5K
 * Total Submissions: 1.6M
 * Testcase Example:  '[1,2,4]\n[1,3,4]'
 *
 * Merge two sorted linked lists and return it as a new list. The new list
 * should be made by splicing together the nodes of the first two lists.
 *
 * Example:
 *
 * Input: 1->2->4, 1->3->4
 * Output: 1->1->2->3->4->4
 *
 *
 */
// @lc code=start
import "fmt"

func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
	if l1 == nil {
		return l2
	}
	if l2 == nil {
		return l1
	}
	if l1.Val < l2.Val {
		tmp := l2
		l2 = l1
		l1 = tmp
	}
	head := l1
	for l1 != nil && l2 != nil {
		if l1.Val > l2.Val {
			tmp1 := l1.Next
			tmp2 := l2.Next
			tmpV := l1.Val
			l1.Val = l2.Val
			l2.Val = tmpV
			l2.Next = tmp1
			l1.Next = l2
			l2 = tmp2
		}
		if l1.Next == nil && l2 != nil {
			l1.Next = l2
			l2 = nil
		}
		l1 = l1.Next
	}
	return head
}

// @lc code=end
