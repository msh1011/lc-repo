package main

/*
* @lc app=leetcode id=29 lang=golang
*
* [29] Divide Two Integers
*
* https://leetcode.com/problems/divide-two-integers/description/
*
* algorithms
* Medium (16.18%)
* Likes:    890
* Dislikes: 4282
* Total Accepted:    236.7K
* Total Submissions: 1.5M
* Testcase Example:  '10\n3'
*
* Given two integers dividend and divisor, divide two integers without using
* multiplication, division and mod operator.
*
* Return the quotient after dividing dividend by divisor.
*
* The integer division should truncate toward zero.
*
* Example 1:
*
*
* Input: dividend = 10, divisor = 3
* Output: 3
*
* Example 2:
*
*
* Input: dividend = 7, divisor = -3
* Output: -2
*
* Note:
*
*
* Both dividend and divisor will be 32-bit signed integers.
* The divisor will never be 0.
* Assume we are dealing with an environment which could only store integers
* within the 32-bit signed integer range: [−2^31,  2^31 − 1]. For the purpose
* of this problem, assume that your function returns 2^31 − 1 when the
* division result overflows.
*
*
 */
import "math"

// @lc code=start

func divide(dividend int, divisor int) int {
	if divisor == 0 {
		return math.MaxInt32
	}
	if divisor == -1 && dividend == math.MinInt32 {
		return math.MaxInt32
	}
	p1 := dividend
	if p1 < 0 {
		p1 = -p1
	}
	p2 := divisor
	if p2 < 0 {
		p2 = -p2
	}
	result := 0
	for p1 >= p2 {
		var numShift uint = 0
		for p1 >= (p2 << numShift) {
			numShift++
		}
		result += 1 << (numShift - 1)
		p1 -= (p2 << (numShift - 1))
	}

	if (dividend > 0 && divisor > 0) || (dividend < 0 && divisor < 0) {
		return result
	} else {
		return -result
	}
}

// @lc code=end
