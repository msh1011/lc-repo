package main

import (
	"fmt"
	"testing"
)

func Test_divide(t *testing.T) {
	tt := []struct {
		input1  int
		input2  int
		output1 int
	}{
		{-2147483648, -1, 2147483647},
		{2147483648, 1, 2147483648},
		{-2147483648, 1, -2147483648},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := divide(tc.input1, tc.input2)
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
