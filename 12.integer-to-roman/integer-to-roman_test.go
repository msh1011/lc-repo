package main

import (
	"fmt"
	"testing"
)

func TestIntToRoman(t *testing.T) {
	tt := []struct {
		input1  int
		output1 string
	}{
		{158, "CLVIII"},
		{1994, "MCMXCIV"},
		{9, "IX"},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := intToRoman(tc.input1)
			if tc.output1 != ret1 {
				t.Logf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
