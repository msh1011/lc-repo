package main

/*
 * @lc app=leetcode id=14 lang=golang
 *
 * [14] Longest Common Prefix
 *
 * https://leetcode.com/problems/longest-common-prefix/description/
 *
 * algorithms
 * Easy (34.51%)
 * Likes:    1879
 * Dislikes: 1599
 * Total Accepted:    611.2K
 * Total Submissions: 1.8M
 * Testcase Example:  '["flower","flow","flight"]'
 *
 * Write a function to find the longest common prefix string amongst an array
 * of strings.
 *
 * If there is no common prefix, return an empty string "".
 *
 * Example 1:
 *
 *
 * Input: ["flower","flow","flight"]
 * Output: "fl"
 *
 *
 * Example 2:
 *
 *
 * Input: ["dog","racecar","car"]
 * Output: ""
 * Explanation: There is no common prefix among the input strings.
 *
 *
 * Note:
 *
 * All given inputs are in lowercase letters a-z.
 *
 */

// @lc code=start
func longestCommonPrefix(strs []string) (ret string) {
	if len(strs) == 0 {
		return
	}
	for i := 0; ; i++ {
		if len(strs[0]) == i {
			return
		}
		c := strs[0][i]
		for _, s1 := range strs {
			if len(s1) == i {
				return
			}
			if s1[i] != c {
				return
			}
		}
		ret += string(c)
	}
}

// @lc code=end
