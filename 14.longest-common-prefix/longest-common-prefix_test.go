package main

import (
	"fmt"
	"testing"
)

func Test_longestCommonPrefix(t *testing.T) {
	tt := []struct {
		input1  []string
		output1 string
	}{
		{[]string{"flower", "flow", "flight"}, "fl"},
		{[]string{"jlower", "flow", "flight"}, ""},
		{[]string{"c", "c"}, "c"},
		{[]string{"", "c"}, ""},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := longestCommonPrefix(tc.input1)
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
