package main

import (
	"fmt"
	"testing"
)

func TestStrStr(t *testing.T) {
	tt := []struct {
		input1 string
			input2 string
		output1 int
	} {
		{},
	} 
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T){
			ret1 := strStr(tc.input1, tc.input2)
			if (tc.output1 != ret1) {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
