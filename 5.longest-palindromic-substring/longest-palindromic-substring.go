package main

type node struct {
	ch   rune
	next *node
}

func longestPalindrome(s string) string {
	maxStr := ""
	if len(s) == 1 {
		return s
	}
	if len(s) == 2 {
		if s[0] != s[1] {
			return s[0:1]
		} else {
			return s
		}
	}
	var stack *node
	for i, c := range s {
		if stack != nil && stack.ch == c {
			ind := i+1
			nlen := 2
			for tmp := stack.next; tmp != nil && ind < len(s); tmp = tmp.next {
				if tmp.ch == rune(s[ind]) {
					nlen += 2
				} else {
					break
				}
				ind++
			}
			if nlen > len(maxStr) {
				maxStr = s[ind-nlen : ind]
			}
		}
		if stack != nil && stack.next != nil && 
			stack.next.ch == c {
			ind := i
			nlen := 1
			for tmp := stack.next; tmp != nil && ind < len(s); tmp = tmp.next {
				if tmp.ch == rune(s[ind]) {
					nlen += 2
				} else {
					break
				}
				ind++
			}
			if nlen > len(maxStr) {
				maxStr = s[ind-nlen : ind]
			}
		}
		s1 := &node{ch: c, next: stack}
		stack = s1
	}
	if len(s) == 0 {
		return ""
	}
	if maxStr == "" {
		return s[0:1]
	}
	return maxStr
}
