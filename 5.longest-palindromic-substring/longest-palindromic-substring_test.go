package main

import (
	"fmt"
	"testing"
	"math/rand"
)

func isPalindrome(input string) bool {
	for i := 0; i < len(input)/2; i++ {
		if input[i] != input[len(input)-i-1] {
			return false
		}
	}
	return true
}

func TestLongestPalindrome(t *testing.T) {
	tt := []struct {
		input string
	} {
		{"bab"},
		{"a"},
		{"aa"},
		{"ac"},
		{"ccc"},
		{"abcda"},
		{"abcba"},
		{"91947779"},
		{"aattttaa"},
		{"aattttaat"},
		{"5080802"},
		{"08080"},
		{"508080"},
		{"6116661458"},
		{"9194777941086746652230821535516129484611666145821"},
	} 
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T){
			s := longestPalindrome(tc.input)
			t.Log(s)
			if !isPalindrome(s) {
				t.Errorf(s)
			}
		})
	}
}

func TestRandomChars(t *testing.T) {
	for i := 0; i < 50; i++ {
		s := fmt.Sprintf("%d%d%d",
			rand.Int(), rand.Int(), rand.Int())
		s1 := longestPalindrome(s)
		if !isPalindrome(s1) {
			t.Log(s)
			t.Errorf(s1)
		}
	}
}
