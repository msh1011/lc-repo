package main

/*
 * @lc app=leetcode id=23 lang=golang
 *
 * [23] Merge k Sorted Lists
 *
 * https://leetcode.com/problems/merge-k-sorted-lists/description/
 *
 * algorithms
 * Hard (37.50%)
 * Likes:    3469
 * Dislikes: 223
 * Total Accepted:    519.4K
 * Total Submissions: 1.4M
 * Testcase Example:  '[[1,4,5],[1,3,4],[2,6]]'
 *
 * Merge k sorted linked lists and return it as one sorted list. Analyze and
 * describe its complexity.
 *
 * Example:
 *
 *
 * Input:
 * [
 * 1->4->5,
 * 1->3->4,
 * 2->6
 * ]
 * Output: 1->1->2->3->4->4->5->6
 *
 *
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func mergeKLists(lists []*ListNode) *ListNode {
	if len(lists) == 0 {
		return nil
	}
	for len(lists) > 1 {
		l1 := lists[0]
		l2 := lists[1]
		lists = lists[2:]
		if l1 == nil {
			lists = append(lists, l2)
			continue
		}
		if l2 == nil {
			lists = append(lists, l1)
			continue
		}
		if l1.Val < l2.Val {
			tmp := l2
			l2 = l1
			l1 = tmp
		}
		head := l1
		for l1 != nil && l2 != nil {
			if l1.Val > l2.Val {
				tmp1 := l1.Next
				tmp2 := l2.Next
				tmpV := l1.Val
				l1.Val = l2.Val
				l2.Val = tmpV
				l2.Next = tmp1
				l1.Next = l2
				l2 = tmp2
			}
			if l1.Next == nil && l2 != nil {
				l1.Next = l2
				l2 = nil
			}
			l1 = l1.Next
		}
		lists = append(lists, head)
	}
	return lists[0]
}

// @lc code=end
