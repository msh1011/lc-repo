package main

import (
	"fmt"
	"testing"
)

func TestMergeKLists(t *testing.T) {
	tt := []struct {
		input1  []*ListNode
		output1 *ListNode
	}{
		{},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := mergeKLists(tc.input1)
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
