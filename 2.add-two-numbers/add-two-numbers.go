package main

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	ret := l1
	co := 0
	for l1 != nil && l2 != nil {
		l1.Val += l2.Val + co
		co = l1.Val / 10
		l1.Val = l1.Val % 10
		if l1.Next == nil && l2.Next != nil {
			l1.Next = &ListNode{Val: 0}
		} else if l2.Next == nil && l1.Next != nil {
			l2.Next = &ListNode{Val: 0}
		} else if l1.Next == nil && l2.Next == nil && co == 1{
			l1.Next = &ListNode{Val: 0}
			l2.Next = &ListNode{Val: 0}
		}
		l1 = l1.Next
		l2 = l2.Next
	}
	return ret
}
