package main

/*
 * @lc app=leetcode id=25 lang=golang
 *
 * [25] Reverse Nodes in k-Group
 *
 * https://leetcode.com/problems/reverse-nodes-in-k-group/description/
 *
 * algorithms
 * Hard (38.99%)
 * Likes:    1609
 * Dislikes: 316
 * Total Accepted:    225.6K
 * Total Submissions: 577.8K
 * Testcase Example:  '[1,2,3,4,5]\n2'
 *
 * Given a linked list, reverse the nodes of a linked list k at a time and
 * return its modified list.
 *
 * k is a positive integer and is less than or equal to the length of the
 * linked list. If the number of nodes is not a multiple of k then left-out
 * nodes in the end should remain as it is.
 *
 *
 *
 *
 * Example:
 *
 * Given this linked list: 1->2->3->4->5
 *
 * For k = 2, you should return: 2->1->4->3->5
 *
 * For k = 3, you should return: 3->2->1->4->5
 *
 * Note:
 *
 *
 * Only constant extra memory is allowed.
 * You may not alter the values in the list's nodes, only nodes itself may be
 * changed.
 *
 *
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
import "fmt"

func reverseKGroup(head *ListNode, k int) *ListNode {
	if head == nil {
		return nil
	}
	if head.Next == nil || k == 1 {
		return head
	}
	n1 := head
	n2 := head
	n3 := head
	for n1 != nil {
		st1 := []int{}
		st2 := []int{}
		i := 0
		for ; i < k/2 && n2 != nil; i++ {
			st1 = append(st1, n2.Val)
			n2 = n2.Next
		}
		for n3 = n2; i < k && n3 != nil; i++ {
			st2 = append(st2, n3.Val)
			n3 = n3.Next
		}
		if len(st2) > 0 && k%2 != 0 {
			st2 = st2[1:]
			n2 = n2.Next
		}
		if len(st1) != k/2 || len(st1) != len(st2) {
			return head
		}
		for len(st1) != 0 {
			n1.Val = st2[len(st1)-1]
			n2.Val = st1[len(st1)-1]
			st1 = st1[:len(st1)-1]
			st2 = st2[:len(st2)-1]
			n1 = n1.Next
			n2 = n2.Next
		}
		n1 = n2
	}
	return head
}

// @lc code=end
