package main

import (
	"fmt"
	"testing"
)

func TestReverseKGroup(t *testing.T) {
	tt := []struct {
		input1 *ListNode
			input2 int
		output1 *ListNode
	} {
		{},
	} 
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T){
			ret1 := reverseKGroup(tc.input1, tc.input2)
			if (tc.output1 != ret1) {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
