package main

/*
* @lc app=leetcode id=9 lang=golang
*
* [9] Palindrome Number
 */
import ()

// @lc code=start
func isPalindrome(x int) bool {
	int_len := func(i int) (c int) {
		for i != 0 {
			i /= 10
			c = c + 1
		}
		return c
	}

	digit_at := func(x, index int) (ret int) {
		ret = x % 10
		for z := 0; z < index; z++ {
			ret = x % 10
			x = x / 10
		}
		return
	}
	if x < 0 {
		return false
	}
	max := int_len(x)
	for z := 1; z < max/2+1; z++ {
		if digit_at(x, z) != digit_at(x, max-z+1) {
			return false
		}
	}
	return true
}

// @lc code=end
