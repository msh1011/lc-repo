package main

import (
	"fmt"
	"testing"
)

func TestIsPalindrome(t *testing.T) {
	tt := []struct {
		input1  int
		output1 bool
	}{
		{42124, true},
		{421224, false},
		{-42124, false},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := isPalindrome(tc.input1)
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
