package main

import (
	"fmt"
	"testing"
)

func Test_removeDuplicates(t *testing.T) {
	tt := []struct {
		input1  []int
		output1 int
	}{
		{[]int{0, 1, 1, 1, 1, 2, 2, 3, 3, 4}, 5},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			t.Log(tc.input1)
			ret1 := removeDuplicates(tc.input1)
			t.Log(tc.input1[:ret1])
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp: %v\ngot: %v", tc.output1, ret1)
			}
		})
	}
}
