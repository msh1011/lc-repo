package main

/*
* @lc app=leetcode id=34 lang=golang
*
* [34] Find First and Last Position of Element in Sorted Array
*
* https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/description/
*
* algorithms
* Medium (34.78%)
* Likes:    2390
* Dislikes: 110
* Total Accepted:    399.3K
* Total Submissions: 1.1M
* Testcase Example:  '[5,7,7,8,8,10]\n8'
*
* Given an array of integers nums sorted in ascending order, find the starting
* and ending position of a given target value.
*
* Your algorithm's runtime complexity must be in the order of O(log n).
*
* If the target is not found in the array, return [-1, -1].
*
* Example 1:
*
*
* Input: nums = [5,7,7,8,8,10], target = 8
* Output: [3,4]
*
* Example 2:
*
*
* Input: nums = [5,7,7,8,8,10], target = 6
* Output: [-1,-1]
*
 */

// @lc code=start
func searchRange(nums []int, target int) (ret []int) {
	ret = append(ret, 0)
	ret = append(ret, len(nums)-1)
	for ret[0] < ret[1] {
		m := ret[0] + (ret[1]-ret[0])/2
		if nums[m] < target {
			ret[0] = m + 1
		} else {
			ret[1] = m
		}
	}
	if ret[0] >= len(nums) || nums[ret[0]] != target {
		ret = []int{-1, -1}
		return
	}
	l := 0
	ret[1] = len(nums) - 1
	for l < ret[1] {
		m := l + (ret[1]-l+1)/2
		if nums[m] > target {
			ret[1] = m - 1
		} else {
			l = m
		}
	}
	return

}

// @lc code=end
