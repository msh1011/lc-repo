package main

import (
	"fmt"
	"testing"
)

func TestSearchRange(t *testing.T) {
	tt := []struct {
		input1  []int
		input2  int
		output1 []int
	}{
		{[]int{5, 7, 7, 8, 8, 10}, 5, []int{0, 0}},
		{[]int{5, 7, 7, 8, 8, 10}, 7, []int{1, 2}},
		{[]int{5, 7, 7, 8, 8, 10}, 8, []int{3, 4}},
		{[]int{5, 7, 7, 8, 8, 10}, 10, []int{5, 5}},
		{[]int{5, 7, 7, 8, 8, 10}, 11, []int{-1, -1}},
		{[]int{}, 11, []int{-1, -1}},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := searchRange(tc.input1, tc.input2)
			if tc.output1[0] != ret1[0] || tc.output1[1] != ret1[1] {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
