package main

import "math"
import "fmt"

func median(arr []int) float64 {
	l := len(arr) / 2
	if len(arr) % 2 == 0 {
		return float64((arr[l]+arr[l+1])) / 2.0
	}
	return float64((arr[l]))
}

func max(x, y float64) float64 {
	if x < y {
		return y
	}
	return x
}

func min(x, y float64) float64 {
	if x > y {
		return y
	}
	return x
}

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	if len(nums1) > len(nums2) {
		nums1, nums2 = nums2, nums1
	}

	x := len(nums1)
	y := len(nums2)

	low := 0
	high := x

	for low <= high {
		partX := (low + high) / 2
		partY := (x + y + 1 ) / 2 - partX

		maxLx := -math.MaxFloat64
		if partX > 0 {
			maxLx = float64(nums1[partX - 1])
		}
		minRx := math.MaxFloat64
		if partX != x {
			minRx = float64(nums1[partX])
		}

		maxLy := -math.MaxFloat64
		if partY > 0 {
			maxLy = float64(nums2[partY - 1])
		}
		minRy := math.MaxFloat64
		if partY != y {
			minRy = float64(nums2[partY])
		}

		if maxLx <= minRy && minRx >= maxLy {
			if (x+y) % 2 == 0 {
				return (max(maxLx, maxLy) + min(minRx, minRy)) / 2.0
			}
			return max(maxLx, maxLy)
		} else if maxLx > minRy {
			high = partX - 1
		} else {
			low = partX + 1
		}
	}
	return 0.0
}
