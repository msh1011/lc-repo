package main

import (
	"fmt"
	"testing"
)

func Test_removeElement(t *testing.T) {
	tt := []struct {
		input1  []int
		input2  int
		output1 int
	}{
		{[]int{0, 1, 2, 2, 3, 0, 4, 2}, 0, 6},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			t.Log(tc.input1)
			ret1 := removeElement(tc.input1, tc.input2)
			t.Log(tc.input1[:ret1])
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
