package main

func lengthOfLongestSubstring(s string) int {
	maxL := 0
	lastPos := make(map[rune]int)
	curL := 0
	for i, ch := range s {
		if lp, ok := lastPos[ch]; ok {
			if i-lp <= curL {
				curL = i - lp
			} else {
				curL++
			}
		} else {
			curL++
		}
		if curL > maxL {
			maxL = curL
		}
		lastPos[ch] = i
	}
	return maxL
}
