package main

import (
	"fmt"
	"testing"
)

func TestBestRotation(t *testing.T) {
	tt := []struct {
		input1 []int
		output1 int
	} {
		{[]int{2,3,1,4,0}, 3},
		{[]int{1,3,0,2,4}, 0},
	} 
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T){
			ret1 := bestRotation(tc.input1)
			if (tc.output1 != ret1) {
				t.Logf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
