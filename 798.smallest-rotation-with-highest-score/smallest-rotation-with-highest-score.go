package main

func bestRotation(A []int) int {
	relArrLoc := func(index int) int {
		if index >= 0 {
			return index
		}
		return len(A)+index
	}
	nScore := make([]int, len(A))
	for x, elem := range A {
		low := (x-elem+1)%len(A)
		high := (x+1)%len(A)
		nScore[relArrLoc(low)]--
		nScore[relArrLoc(high)]++
		if low > high {
			nScore[0]--
		}
	}
	max := -len(A)
	ret := 0
	cur := 0
	for x, elem := range nScore {
		cur += elem
		if cur > max {
			max = cur
			ret = x
		}
	}
	return ret
}
