package main

import (
	"fmt"
	"testing"
)

func TestFindSubstring(t *testing.T) {
	tt := []struct {
		input1  string
		input2  []string
		output1 []int
	}{
		{"lingmindraboofooowingdingbarrwingmonkeypoundcake", []string{"fooo", "barr", "wing", "ding", "wing"}, []int{13}},
		{"barfoothefoobarman", []string{"foo", "bar"}, []int{0, 9}},
		{"barfoothefoobarman", []string{"foo", "bar", "man"}, []int{9}},
		{"", []string{"foo", "bar"}, []int{}},
		{"barfoothefoobarman", []string{}, []int{}},
		{"", []string{}, []int{}},
		{"barfoofoobarthefoobarman", []string{"bar", "foo", "the"}, []int{6, 9, 12}},
		{"wordgoodgoodgoodbestword", []string{"word", "good", "best", "word"}, []int{}},
		{"wordgoodgoodgoodbestword", []string{"word", "good", "best", "good"}, []int{8}},
		{"ababaab", []string{"ab", "ba", "ba"}, []int{1}},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := findSubstring(tc.input1, tc.input2)
			t.Log(ret1)
			if len(ret1) != len(tc.output1) {
				t.Errorf("Error:\nexp: %v\ngot: %v\n", tc.output1, ret1)
				return
			}
			for i := range ret1 {
				if ret1[i] != tc.output1[i] {
					t.Errorf("Error:\nexp: %v\ngot: %v\n", tc.output1, ret1)
				}
			}
		})
	}
}
