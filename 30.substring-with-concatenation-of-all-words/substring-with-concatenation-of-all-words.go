package main

/*
 * @lc app=leetcode id=30 lang=golang
 *
 * [30] Substring with Concatenation of All Words
 *
 * https://leetcode.com/problems/substring-with-concatenation-of-all-words/description/
 *
 * algorithms
 * Hard (24.55%)
 * Likes:    696
 * Dislikes: 1080
 * Total Accepted:    157.3K
 * Total Submissions: 640.1K
 * Testcase Example:  '"barfoothefoobarman"\n["foo","bar"]'
 *
 * You are given a string, s, and a list of words, words, that are all of the
 * same length. Find all starting indices of substring(s) in s that is a
 * concatenation of each word in words exactly once and without any intervening
 * characters.
 *
 *
 *
 * Example 1:
 *
 *
 * Input:
 * ⁠ s = "barfoothefoobarman",
 * ⁠ words = ["foo","bar"]
 * Output: [0,9]
 * Explanation: Substrings starting at index 0 and 9 are "barfoo" and "foobar"
 * respectively.
 * The output order does not matter, returning [9,0] is fine too.
 *
 *
 * Example 2:
 *
 *
 * Input:
 * ⁠ s = "wordgoodgoodgoodbestword",
 * ⁠ words = ["word","good","best","word"]
 * Output: []
 *
 *
 */

// @lc code=start
import ()

func findSubstring(str string, words []string) (ret []int) {
	if len(words) == 0 || str == "" {
		return
	}
	freq := map[string]int{}
	for _, w := range words {
		freq[w]++
	}
	length := len(words[0])
	for j := 0; j < length; j++ {
		cMap := map[string]int{}
		start := j
		count := 0
		for i := j; i <= len(str)-length; i = i + length {
			sub := str[i : i+length]
			if freq[sub] <= 0 {
				cMap = map[string]int{}
				start = i + length
				count = 0
				continue
			}
			cMap[sub]++
			count++
			for cMap[sub] > freq[sub] {
				l := str[start : start+length]
				cMap[l]--
				count--
				start = start + length
			}
			if count == len(words) {
				ret = append(ret, start)
				l := str[start : start+length]
				cMap[l]--
				count--
				start = start + length
			}
		}
	}
	return
}

// @lc code=end
