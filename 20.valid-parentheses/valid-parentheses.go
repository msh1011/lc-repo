package main

/*
 * @lc app=leetcode id=20 lang=golang
 *
 * [20] Valid Parentheses
 *
 * https://leetcode.com/problems/valid-parentheses/description/
 *
 * algorithms
 * Easy (37.79%)
 * Likes:    3856
 * Dislikes: 190
 * Total Accepted:    801.9K
 * Total Submissions: 2.1M
 * Testcase Example:  '"()"'
 *
 * Given a string containing just the characters '(', ')', '{', '}', '[' and
 * ']', determine if the input string is valid.
 *
 * An input string is valid if:
 *
 *
 * Open brackets must be closed by the same type of brackets.
 * Open brackets must be closed in the correct order.
 *
 *
 * Note that an empty string is also considered valid.
 *
 * Example 1:
 *
 *
 * Input: "()"
 * Output: true
 *
 *
 * Example 2:
 *
 *
 * Input: "()[]{}"
 * Output: true
 *
 *
 * Example 3:
 *
 *
 * Input: "(]"
 * Output: false
 *
 *
 * Example 4:
 *
 *
 * Input: "([)]"
 * Output: false
 *
 *
 * Example 5:
 *
 *
 * Input: "{[]}"
 * Output: true
 *
 *
 */

// @lc code=start
func isValid(s string) bool {
	var t1 uint16 = 0
	var t2 uint16 = 0
	var t3 uint16 = 0
	st := []int8{}
	for _, c := range s {
		if c == '(' {
			t1++
			st = append(st, 1)
		} else if c == ')' {
			t1--
			i := len(st) - 1
			if i < 0 || st[i] != 1 {
				return false
			}
			st = st[:i]
		} else if c == '{' {
			t2++
			st = append(st, 2)
		} else if c == '}' {
			t2--
			i := len(st) - 1
			if i < 0 || st[i] != 2 {
				return false
			}
			st = st[:i]
		} else if c == '[' {
			t3++
			st = append(st, 3)
		} else if c == ']' {
			t3--
			i := len(st) - 1
			if i < 0 || st[i] != 3 {
				return false
			}
			st = st[:i]
		}
	}
	if len(st) != 0 {
		return false
	}
	return true
}

// @lc code=end
