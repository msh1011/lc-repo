package main

import (
	"fmt"
	"testing"
)

func TestIsValid(t *testing.T) {
	tt := []struct {
		input1  string
		output1 bool
	}{
		{"{}{{{{[]}}}}", true},
		{"{}((([]))){{{{[]}}}}", true},
		{"{}(([]))){{{{[]}}}}", false},
		{"{}((([)))){{{{[]}}}}", false},
		{"{", false},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := isValid(tc.input1)
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
