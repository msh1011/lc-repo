package main

/*
* @lc app=leetcode id=32 lang=golang
*
* [32] Longest Valid Parentheses
*
* https://leetcode.com/problems/longest-valid-parentheses/description/
*
* algorithms
* Hard (27.00%)
* Likes:    2630
* Dislikes: 116
* Total Accepted:    239.8K
* Total Submissions: 886.6K
* Testcase Example:  '"(()"'
*
* Given a string containing just the characters '(' and ')', find the length
* of the longest valid (well-formed) parentheses substring.
*
* Example 1:
*
*
* Input: "(()"
* Output: 2
* Explanation: The longest valid parentheses substring is "()"
*
*
* Example 2:
*
*
* Input: ")()())"
* Output: 4
* Explanation: The longest valid parentheses substring is "()()"
*
*
 */
import "fmt"

// @lc code=start
func longestValidParentheses(s string) (max int) {
	for i := 0; i < len(s); i++ {
		t1 := 0
		l := 0
		for _, c := range s[i:] {
			if c == '(' {
				t1++
			} else {
				t1--
				if t1 < 0 {
					break
				}
			}
			l++
			if t1 == 0 && l > max {
				max = l
			}
		}
	}
	return
}

// @lc code=end
