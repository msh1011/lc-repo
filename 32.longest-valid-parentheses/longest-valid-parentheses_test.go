package main

import (
	"fmt"
	"testing"
)

func TestLongestValidParentheses(t *testing.T) {
	tt := []struct {
		input1  string
		output1 int
	}{
		{"(()", 2},
		{"(", 0},
		{"", 0},
		{"))))))", 0},
		{")())", 2},
		{"())()())()", 4},
		{"))(())()((((((())(()))((())(((((((()))())((((())())(()())))))))))((()((()(()(()()((()()()(()()()))(()()(()(())())))()())()(((((", 54},
		{")(())))(())())", 6},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := longestValidParentheses(tc.input1)
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
