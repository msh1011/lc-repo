package main

/*
* @lc app=leetcode id=15 lang=golang
*
* [15] 3Sum
*
* https://leetcode.com/problems/3sum/description/
*
* algorithms
* Medium (25.46%)
* Likes:    5235
* Dislikes: 636
* Total Accepted:    739K
* Total Submissions: 2.9M
* Testcase Example:  '[-1,0,1,2,-1,-4]'
*
* Given an array nums of n integers, are there elements a, b, c in nums such
* that a + b + c = 0? Find all unique triplets in the array which gives the
* sum of zero.
*
* Note:
*
* The solution set must not contain duplicate triplets.
*
* Example:
*
*
* Given array nums = [-1, 0, 1, 2, -1, -4],
*
* A solution set is:
* [
* ⁠ [-1, 0, 1],
* ⁠ [-1, -1, 2]
* ]
*
 */

import (
	"sort"
)

// @lc code=start
func threeSum(nums []int) (ret [][]int) {
	dupl := func(n1 []int) bool {
		for _, n2 := range ret {
			if n1[0] == n2[0] && n1[1] == n2[1] && n1[2] == n2[2] {
				return true
			}
		}
		return false
	}
	if len(nums) < 3 {
		return
	}
	sort.Ints(nums)
	for i := range nums[:len(nums)-2] {
		l := i + 1
		r := len(nums) - 1
		for l < r {
			s := nums[i] + nums[l] + nums[r]
			if s == 0 {
				arr := []int{nums[i], nums[l], nums[r]}
				if !dupl(arr) {
					ret = append(ret, arr)
				}
				r -= 1
			} else if s < 0 {
				l += 1
			} else {
				r -= 1
			}
		}
	}
	return
}

// @lc code=end
