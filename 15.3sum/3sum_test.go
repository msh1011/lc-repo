package main

import (
	"fmt"
	"testing"
)

func TestThreeSum(t *testing.T) {
	tt := []struct {
		input1  []int
		output1 [][]int
	}{
		{[]int{-1, 0, 1, 2, -1, -4}, [][]int{[]int{-1, 0, 1}, []int{-1, -1, 2}}},
		{[]int{}, [][]int{}},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := threeSum(tc.input1)
			t.Log(ret1)
		})
	}
}
