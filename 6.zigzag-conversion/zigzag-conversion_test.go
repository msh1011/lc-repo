package main

import (
	"fmt"
	"testing"
)

func TestConvert(t *testing.T) {
	tt := []struct {
		input string
		rows int
	} {
		{"PAYPALISHIRING", 4},
		{"PAYPALISHIRING", 3},
		{"PAYPALISHIRING", 1},
	} 
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T){
			ret := convert(tc.input, tc.rows)
			t.Log(ret)
		})
	}
}
