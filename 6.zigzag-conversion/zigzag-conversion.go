package main

func convert(s string, numRows int) string {
	if numRows <= 1 {
		return s
	}
	retStr := make([]string, numRows)
	var down bool
	var row int
	for _, el := range s {
		retStr[row] += string(el)
		if (row == numRows-1) {
			down = false
		} else if (row == 0) {
			down = true
		}
		if down {
			row++
		} else {
			row--
		}
	}
	str := ""
	for _, e := range retStr {
		str += e
	}
	return str
}
