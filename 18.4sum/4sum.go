package main

/*
* @lc app=leetcode id=18 lang=golang
*
* [18] 4Sum
*
* https://leetcode.com/problems/4sum/description/
*
* algorithms
* Medium (32.15%)
* Likes:    1442
* Dislikes: 274
* Total Accepted:    285.6K
* Total Submissions: 887.7K
* Testcase Example:  '[1,0,-1,0,-2,2]\n0'
*
* Given an array nums of n integers and an integer target, are there elements
* a, b, c, and d in nums such that a + b + c + d = target? Find all unique
* quadruplets in the array which gives the sum of target.
*
* Note:
*
* The solution set must not contain duplicate quadruplets.
*
* Example:
*
*
* Given array nums = [1, 0, -1, 0, -2, 2], and target = 0.
*
* A solution set is:
* [
* ⁠ [-1,  0, 0, 1],
* ⁠ [-2, -1, 1, 2],
* ⁠ [-2,  0, 0, 2]
* ]
*
*
 */

import (
	"sort"
)

// @lc code=start
func fourSum(nums []int, target int) (ret [][]int) {
	if len(nums) < 4 {
		return
	}
	sort.Ints(nums)
	for i := 0; i < len(nums)-3; i++ {
		if i != 0 && nums[i] == nums[i-1] {
			continue
		}
		for j := i + 1; j < len(nums)-2; j++ {
			if j != i+1 && nums[j] == nums[j-1] {
				continue
			}
			k := j + 1
			l := len(nums) - 1
			for k < l {
				s := nums[i] + nums[j] + nums[k] + nums[l]
				if s < target {
					k++
				} else if s > target {
					l--
				} else {
					ret = append(ret, []int{nums[i], nums[j], nums[k], nums[l]})

					k++
					l--

					for k < l && nums[l] == nums[l+1] {
						l--
					}

					for k < l && nums[k] == nums[k-1] {
						k++
					}
				}
			}
		}
	}
	return
}

// @lc code=end
