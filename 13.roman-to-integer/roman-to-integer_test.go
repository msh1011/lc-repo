package main

import (
	"fmt"
	"testing"
)

func TestRomanToInt(t *testing.T) {
	tt := []struct {
		input1  string
		output1 int
	}{
		{"MCMXCIV", 1994},
		{"MCMXCVI", 1996},
		{"", 0},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := romanToInt(tc.input1)
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
