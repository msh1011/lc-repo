package main

import (
	"fmt"
	"testing"
)

func TestReverse(t *testing.T) {
	tt := []struct {
		input1  int
		output1 int
	}{
		{123, 321},
		{-123, -321},
		{-1230, -321},
		{120, 21},
		{1534236469, 0},
	}
	for i, tc := range tt {
		t.Run(fmt.Sprintf("Test-%d", i), func(t *testing.T) {
			ret1 := reverse(tc.input1)
			if tc.output1 != ret1 {
				t.Errorf("Error: \nexp:\n%v\ngot:\n%v", tc.output1, ret1)
			}
		})
	}
}
