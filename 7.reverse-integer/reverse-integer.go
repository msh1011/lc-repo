package main

/*
 * @lc app=leetcode id=7 lang=golang
 *
 * [7] Reverse Integer
 */
import (
	"strconv"
)

// @lc code=start
func reverse(x int) int {
	var stack []rune
	ret := ""
	if x < 0 {
		ret = "-"
	}
	s := strconv.Itoa(x)
	for _, char := range s {
		stack = append(stack, char)
	}

	lz := true
	for len(stack) > 0 {
		n := len(stack) - 1
		elem := string(stack[n])
		if (!lz || elem != "0") && elem != "-" {
			lz = false
			ret += elem
		}
		stack = stack[:n]
	}
	retI, _ := strconv.Atoi(ret)
	if retI > 1<<31-1 || retI < -1<<31 {
		return 0
	}
	return retI
}

// @lc code=end
